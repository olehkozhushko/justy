import { defineStore } from "pinia";

type Todo = {
    id: number,
    title: string,
    completed: boolean
}

interface State {
    todos: Todo[]
}

export const useTodosStore = defineStore("todos", {
    state: (): State => ({
        todos: []
    }),
    actions: {
        async getTodos() {
            const resp = await fetch("https://jsonplaceholder.typicode.com/todos");
            const todos = await resp.json();

            this.todos = todos;
        }
    }
})