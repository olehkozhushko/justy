import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const loading = ref(false);

  const doubleCount = computed(() => count.value * 2)

  function increment() {
    count.value++
  }

  function setIsLoading(state: boolean) {
    loading.value = state;
    
    if(state) {
      setTimeout(() => {
        loading.value = false;
      }, 3000)
    }
  }

  return { count, doubleCount, loading, increment, setIsLoading }
})
